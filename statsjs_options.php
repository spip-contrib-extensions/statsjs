<?php

/**
 * Préparer le comptage et inhiber le comptage du core
 *
 * @param array $entetes
 * @return array
 */
function statsjs_affichage_entetes_final($entetes) {
	$GLOBALS['compte_stats'] = ($GLOBALS['meta']['activer_statistiques'] != 'non') ? true : false;

	if ($GLOBALS['compte_stats']) {
		$html = preg_match(',^\s*text/html,', $entetes['Content-Type']);

		// decomptage des visites, on peut forcer a oui ou non avec le header X-Spip-Visites
		// par defaut on ne compte que les pages en html (ce qui exclue les js,css et flux rss)
		$spip_compter_visites = ($html ? 'oui' : 'non');
		if (isset($entetes['X-Spip-Visites'])) {
			$spip_compter_visites = in_array($entetes['X-Spip-Visites'], ['oui','non'])
				? $entetes['X-Spip-Visites']
				: $spip_compter_visites;
			unset($entetes['X-Spip-Visites']);
		}

		// Gestion des statistiques du site public
		if ($spip_compter_visites == 'non') {
			$GLOBALS['compte_stats'] = false;
		}
	}

	// dans tous les cas on désactive le passage dans stats_affichage_entetes_final du plugin-dist
	defined('_STATS_INHIB_COMPTER_VISITES_AFFICHAGE_ENTETES_FINAL') || define('_STATS_INHIB_COMPTER_VISITES_AFFICHAGE_ENTETES_FINAL', true);
	return $entetes;
}

/**
 * Insérer le mouchard si besoin
 * @param string $page
 * @return string
 */
function statsjs_affichage_final($page) {
	if (
		isset($GLOBALS['compte_stats'])
		&& $GLOBALS['compte_stats']
		// Pas de double insertion si un marqueur JS a été inséré à la main par le squelette
		&& strpos($page, '<!-- Stats SPIP-JS -->') === false
		// pas de marqueur JS sur les pages amp (horreur malheur !...)
		&& strpos($page, '</amp-') === false
	) {
		if (
			($p = stripos($page, '</body>')) !== false
			&& !empty($GLOBALS['html'])
			// pas de marqueur JS sur les pages 404
			&& (!isset($GLOBALS['page']['status']) || $GLOBALS['page']['status'] !== 404)
		) {
			// recuperer et injecter le mouchard (surchargeable) qui dépend du contexte
			if (function_exists($f = 'statsjs_mouchard') || function_exists($f .= '_dist')) {
				$js = $f($GLOBALS['contexte']);
				$page = substr_replace($page, $js, $p, 0);
			}
		}
	}

	return $page;
}

/**
 * Générer le mouchard
 * La fonction est surchargeable pour permettre d'utiliser un autre mouchard perso (matomo ou autre)
 * @param array $contexte
 * @return string
 */
function statsjs_mouchard_dist($contexte) {
	// extraire les id_xx du contexte, c'est la seule chose qui nous interesse pour compter les stats
	// le referer sera ajouté en JS dans l'url de l'action
	$c = [];
	foreach ($contexte as $k => $v) {
		if (strncmp($k, 'id_', 3) == 0 and is_numeric($v)) {
			$c[$k] = $v;
		}
	}
	$c = json_encode($c);
	$c = base64_encode($c);
	$u = protocole_implicite(generer_url_action('statsjs', "c=$c", true));
	$js = <<<js
<!-- Stats SPIP-JS -->
<script type="text/javascript">
(function() {
var w=window,d=document,r="",g=d.createElement('script'),s=d.getElementsByTagName('script')[0];
try{r=w.top.document.referrer}catch(Y){if(w.parent){try{r=w.parent.document.referrer}catch(X){r=""}}}if(r===""){r=d.referrer}
g.type='text/javascript';g.defer=true;g.async=true;g.src="$u"+"&r="+encodeURIComponent(r);s.parentNode.insertBefore(g,s);
})();
</script>
js;

	return $js;
}

/**
 * L'action qui compte les visites appelées par le JS
 * via le mouchard
 * @return void
 */
function action_statsjs_dist() {
	// no-content et fermet la connection http
	http_response_code(204); // No Content
	header('X-Robots-Tag: noindex');
	header('Connection: close');

	// sauver le contexte pour d'éventuels crons en fin de hit
	$save_contexte = $GLOBALS['contexte'] ?? [];
	$save_referer = $_SERVER['HTTP_REFERER'] ?? null;

	// puis compter :
	if (
		($c = _request('c'))
		&& ($c = base64_decode($c, true))
		&& ($c = json_decode($c, true))
	) {
		$GLOBALS['contexte'] = $c;
	}
	if ($r = _request('r')) {
		$_SERVER['HTTP_REFERER'] = rawurldecode($r);
	}

	// on délègue au core qui fait ça bien
	$stats = charger_fonction('stats', 'public');
	$stats();

	// restaurer le contexte
	$GLOBALS['contexte'] = $save_contexte;
	$_SERVER['HTTP_REFERER'] = $save_referer;
}
