# StatsJS

Ce plugin permet de compter les statistiques via un marqueur JS au lieu de compter via le hit PHP (fonctionnement par défaut de SPIP)

Ce fonctionnement à plusieurs intérêts :
* il permet d'utiliser un proxy-cache comme Varnish en frontal du site SPIP
pour mettre en cache certaines pages de SPIP pendant un certain temps sans fausser le comptage des stats
* les visiteurs sans JS activé (qui n'évaluent pas le JS de la page) ne déclenchent pas le hit de comptage des stats,
ce qui exclue naturellement la plupart des bots, y compris ceux qui se font passer pour des utilisateurs humains
en utilisant le UA d'un navigateur standard.

A noter :
* seule la méthode de déclenchement du comptage change (via un hit lancé en JS au lieu du hit PHP),
mais la méthode de comptage et sa logique ne changent pas
et restent celles du plugin [Statistiques](https://git.spip.net/spip/statistiques/) de SPIP
* les éventuels Bots qui executent du JS mais ne trafiquent par leur UA (ce qui ne concerne à peu près que G*****Bot)
restent non comptés https://git.spip.net/spip/statistiques/-/blob/master/public/stats.php?ref_type=heads#L58


## Fonctionnement

Le code du mouchard JS est généré par la fonction surchargeable `statsjs_mouchard_dist()`.
Celle-ci reçoit le contexte de la page en argument et doit retourner un morceau de HTML à insérer dans le pied de page.

La fonction génère par défaut un bout de JS qui va appeler l'url `?action=statsjs` en lui passant
* tous les `id_*` du contexte dans un tableau `json_encode` + `base64_encode`
* le referer de la page, récupéré en JS dans la page courante

La fonction `action_statsjs_dist()` surchargeable récupère le contexte
et le referer et les injecte respectivement dans `$GLOBALS['contexte']` et `$_SERVER['HTTP_REFERER']` avant d'appeler
la fonctiond de comptage du core

## Personalisation

Le code du mouchard peut être modifié en déclarant une fonction `statsjs_mouchard()` pour remplacer par exemple le JS
du plugin par le JS d'appel d'un compteur matomo ou de tout autre logiciel de comptage de visites.
